import configparser
import datetime as dt
import numpy as np
import pandas as pd
from loguru import logger
from pathlib import Path
from typing import Union
import win32com.client as win32
import random

class Table(object):

    def __init__(self, config: dict = None, config_path: str = None):
        self.logger = logger
        self.config = config if config else self.load_config(config_path=config_path)
        self.participants = self.load_participants()
        self.tables = self.assign()

    def load_config(self, config_path: Union[str, Path]) -> configparser.RawConfigParser:
        """
        Loads the config file.

        Note:
            In case of a FileNotFoundError due to the nature of network drives this could also be a hint towards an
            incorrect or missing drive mapping.

        Args:
            config_path (Union[str, Path], optional): If provided, specifies the config file path to be loaded.

        Returns:
            configparser.RawConfigParser: The loaded config.

        Raises:
            FileNotFoundError: If the specified config file could not be found at the target location.
        """

        if not config_path.exists():
            error_msg = f'Attempting to load the config file {config_path}, while this file could not be found!'
            self.logger.error(error_msg)
            raise FileNotFoundError(error_msg)

        config = configparser.RawConfigParser()
        config.read(str(config_path))

        return config

    def assign(self):
        group_ls = self.participants.index.to_list()
        z = dt.date.today()
        m = z - dt.timedelta(z.isoweekday()-1)
        m_seed = m.strftime("%Y%m%d")
        random.seed(m_seed)
        group_ls = random.sample(group_ls, len(group_ls))
        return group_ls

    def load_participants(self):
        parti_file = Path(self.config['paths']['Participants'])
        participants_df = pd.read_csv(parti_file, sep=',')
        participants_df['Index'] = np.arange(len(participants_df)) + 1
        participants_df = participants_df.set_index('Index')
        self.logger.debug("Participants fetched successfully")
        return participants_df

    def output_participants(self):
        print(self.participants.to_string())

    def send_invite(self):
        table_size = int(self.config['params']['table size'])
        if len(self.tables) % table_size == 0:
            for i in range(0, len(self.tables), table_size):
                ls = []
                ls_table = []
                for k in range(0, table_size):
                    ls.append(i + k)
                    ls_table.append(self.tables[i + k])
                self.send_mail(ls_table)
        elif len(self.tables) % table_size == 1:
            for i in range(0, len(self.tables)-1, table_size):
                ls = []
                ls_table = []
                for k in range(0, table_size):
                    ls.append(k)
                    ls_table.append(self.tables[i + k])
                if (i+table_size) == (len(self.tables) - 1):
                    ls_table.append(self.tables[i + table_size])
                self.send_mail(ls_table)
        elif len(self.tables) % table_size == 2:
            if len(self.tables) >= 10:
                for i in range(0, len(self.tables) - 2, table_size):
                    ls = []
                    ls_table = []
                    for k in range(0, table_size):
                        ls.append(k)
                        ls_table.append(self.tables[i + k])
                    if i == (len(self.tables) - 2 - 8):
                        ls_table.append(self.tables[-2])
                    elif i == (len(self.tables) - 2 - 4):
                        ls_table.append(self.tables[-1])
                    self.send_mail(ls_table)
            elif len(self.tables) == 6:
                ls_table = self.tables
                self.send_mail(ls_table)
            else:
                ls_table = self.tables
                self.send_mail(ls_table)
        elif len(self.tables) % table_size == 3:
            if len(self.tables) >= 15:
                for i in range(0, len(self.tables) - 3, table_size):
                    ls = []
                    ls_table = []
                    for k in range(0, table_size):
                        ls.append(k)
                        ls_table.append(self.tables[i + k])
                    if i == (len(self.tables) - 3 - 12):
                        ls_table.append(self.tables[-3])
                    elif i == (len(self.tables) - 3 - 8):
                        ls_table.append(self.tables[-2])
                    elif i == (len(self.tables) - 3 - 4):
                        ls_table.append(self.tables[-1])
                    self.send_mail(ls_table)
            elif len(self.tables)  == 11:
                for i in range(0, len(self.tables) - 3, table_size):
                    ls = []
                    ls_table = []
                    for k in range(0, table_size):
                        ls.append(k)
                        ls_table.append(self.tables[i + k])
                    if i == (len(self.tables) - 3 - 8):
                        ls_table.append(self.tables[-3])
                    elif i == (len(self.tables) - 3 - 4):
                        ls_table.append(self.tables[-2])
                        ls_table.append(self.tables[-1])
                    self.send_mail(ls_table)
            elif len(self.tables) == 7:
                ls_table = self.tables
                self.send_mail(ls_table)
            else:
                ls_table = self.tables
                self.send_mail(ls_table)
        self.logger.info("Invite Mails out please press send")

    def send_mail(self, ls_table):
        group = ""
        for l in ls_table:
            full_name = self.participants.loc[l]['Last_Name Name']
            group = group + f"{full_name} <br>  \
                            "
        for j in ls_table:
            address = self.participants.loc[j]['Email']
            self.send_mail_template(address, '', 'Random Lunch Invitation', group, send_directly=False)


    def send_mail_template(self, address, cc, subject, body, send_directly=False):
        """
        This function sends emails to spcific address
        :param address      : Email address         (string)
        :param cc           : Email address         (string)
        :param subject      : Email subject         (string)
        :return             : None
        """
        outlook = win32.Dispatch('outlook.application')
        mail = outlook.CreateItem(0)
        mail.To = address
        mail.CC = cc
        mail.Subject = subject
        date_thursday = (dt.date.today() - dt.timedelta(dt.date.today().isoweekday()-1) + dt.timedelta(3)).strftime("%d.%m.%Y")
        mail.HTMLbody = f'<font face = "Calibri"> Dear all, <br> \
                    <br> \
                    You are invited for this week\'s intern random lunch together with: <br> \
                    <br> \
                    {body} <br> \
                    Meeting point is 1pm on Thursday {date_thursday} in front of the elevator: <br> \
                    <br> \
                    Kind regards,  <br> \
                    <br> \
                    ---------------------------------------------------- <br> \
                    Solactive AG <br> \
                    <br> \
                    Direct: +49 (69) 719 160 00 <br> \
                    <br> \
                    Address: Platz der Einheit 1, 60327 Frankfurt, Germany <br> \
                    Fax: +49 (69) 719 160 25 <br> \
                    <a href=""solactive.com"">solactive.com</a> <br> \
                    <br> \
                    Solactive AG, Sitz Frankfurt am Main, Amtsgericht Frankfurt am Main, HRB 79986 <br> \
                    Head of Supervisory Body: Dr. Felix Mühlhäuser <br> \
                    Management Board: Steffen Scheuble, Christian Vollmuth, Dirk Urmoneit, Timo Pfeifefr <br> \
                    ---------------------------------------------------- <br></font>'
        mail.Display(False)
        if send_directly:
            mail.send

