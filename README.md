# Random Lunch

Will send invites for lunch to a set number of people in order for them to enjoy lunch together.
This project can be used to e.g. enable employees to meet colleagues from other departments.