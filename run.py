from randomlunch.table import Table
from pathlib import Path


if __name__ == '__main__':
    config_path = Path('config') / 'config.ini'
    table_pool = Table(config_path=config_path)
    print(table_pool.participants.to_string())
    # print(table_pool.tables)
    table_pool.send_invite()
    # table_pool.send_mail_template("lam.anhtruc@gmail.com", "lam.anhtruc@gmail.com", "test", "heyhh", send_directly=False)